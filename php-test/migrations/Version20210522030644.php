<?php

declare(strict_types=1);

namespace MyProject\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210522030644 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $table = $schema->createTable('users');
        $table->addColumn('id', 'integer', array('unsigned' => true));
        $table->addColumn('nome', 'string');
        $table->addColumn('sobrenome', 'string');
        $table->addColumn('cpf', 'string', array('lenght' => 11, 'unique' => true));
        $table->addColumn('email', 'string', array('nullable' => true);
        $table->addColumn('password', 'string');
        $table->addColumn('cnpj', 'boolean');
        $table->addColumn('formacao', 'text');
        $table->addColumn('experiencia', 'text');
        $table->setPrimaryKey(array('id'));

        $table2 = $schema->createTable('servicos');
        $table2->addColumn('id', 'integer', array('unsigned' => true));
        $table2->addColumn('nome', 'string');
        $table2->setPrimaryKey(array('id'));

        $table3 = $schema->createTable('users_servicos');
        $table3->addColumn('id', 'integer', array('unsigned' => true));
        $table3->addIndex('users_servicos', 'user_id_fk', array(
            'fields'=>array('user_id')
        ));
        $table3->createForeignKey('users_servicos', 'user_id_fk', array(
                'local' => 'user_id',
                'foreign' => 'id',
                'foreignTable' => 'users',
                'onDelete'=>'CASCADE'
        ));
        $table3->addIndex('users_servicos', 'servico_id_fk', array(
            'fields'=>array('servico_id')
        ));
        $table3->createForeignKey('users_servicos', 'servico_id_fk', array(
                'local' => 'servico_id',
                'foreign' => 'id',
                'foreignTable' => 'users',
                'onDelete'=>'CASCADE'
        ));
        $table3->setPrimaryKey(array('id'));

    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('user');
        $schema->dropTable('servicos');
        $schema->dropForeignKey('users_servicos', 'user_id_fk');
        $schema->removeIndex('users_servicos', 'user_id_fk');
        $schema->dropForeignKey('users_servicos', 'servico_id_fk');
        $schema->removeIndex('users_servicos', 'servico_id_fk');
        $schema->dropTable('users_servicos');

    }
}
