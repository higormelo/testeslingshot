<?php

declare(strict_types=1);

/**
 * Class SubstitutionEncodingAlgorithm
 */
class SubstitutionEncodingAlgorithm implements EncodingAlgorithm
{
    /**
     * @var array
     */
    private $substitutions;

    /**
     * SubstitutionEncodingAlgorithm constructor.
     * @param $substitutions
     */
    public function __construct(array $substitutions)
    {
        $this->substitutions = $substitutions;
    }

    /**
     * Encodes text by substituting character with another one provided in the pair.
     * For example pair "ab" defines all "a" chars will be replaced with "b" and all "b" chars will be replaced with "a"
     * Examples:
     *      substitutions = ["ab"], input = "aabbcc", output = "bbaacc"
     *      substitutions = ["ab", "cd"], input = "adam", output = "bcbm"
     *
     * @param string $text
     * @return string
     */
    public function encode(string $text): string
    {
        $arrayCheck = []; // is used to check the first carachter of the substitution string
        foreach ($this->substitutions as $substitution) {
            // String validation
            if (strlen($substitution) != 2 || $substitution[0] == $substitution[1] || in_array($substitution[0], $arrayCheck)) {
                throw new \InvalidArgumentException('Something is wrong');
            }
            array_push($arrayCheck, $substitution[0]); 
            $arrayTranslation = [$substitution[0] => $substitution[1], 
                        $substitution[1] => $substitution[0],
                        strtoupper($substitution[0]) => strtoupper($substitution[1]),
                        strtoupper($substitution[1]) => strtoupper($substitution[0])
                    ];
            $text = strtr($text, $arrayTranslation);
        }

        return $text;
    }
}
