<?php

declare(strict_types=1);

/**
 * Class OffsetEncodingAlgorithm
 */
class OffsetEncodingAlgorithm implements EncodingAlgorithm
{
    /**
     * Lookup string
     */
    public const CHARACTERS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    /**
     * @var int
     */
    private $offset;

    /**
     * @param int $offset
     */
    public function __construct(int $offset = 13)
    {
        if ($offset < 0) {
            throw new \InvalidArgumentException('Something is wrong');
        }
        $this->offset = $offset;
    }

    /**
     * Encodes text by shifting each character (existing in the lookup string) by an offset (provided in the constructor)
     * Examples:
     *      offset = 1, input = "a", output = "b"
     *      offset = 2, input = "z", output = "B"
     *      offset = 1, input = "Z", output = "a"
     *
     * @param string $text
     * @return string
     */
    public function encode(string $text = null): string
    {
        $text = str_split($text);
        foreach ($text as $key => $character) {
            if (!empty($character) && strpos(self::CHARACTERS, $character) !== false) {
                $text[$key] = self::CHARACTERS[(strpos(self::CHARACTERS, $character) + $this->offset) % 52];
            }
        }
        $text = implode('', $text);
        return $text;
    }
}
