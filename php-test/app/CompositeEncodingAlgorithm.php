<?php

declare(strict_types=1);

/**
 * Class CompositeEncodingAlgorithm
 */
class CompositeEncodingAlgorithm implements EncodingAlgorithm
{
    /**
     * @var EncodingAlgorithm[]
     */
    private $algorithms;

    /**
     * CompositeEncodingAlgorithm constructor
     */
    public function __construct()
    {
        $this->algorithms = [];
    }

    /**
     * @param EncodingAlgorithm $algorithm
     */
    public function add(EncodingAlgorithm $algorithm): void
    {
        // array_push($this->algorithms, $algorithm); 
        $this->algorithms[] = $algorithm;
    }

    /**
     * Encodes text using multiple Encoders (in orders encoders were added)
     *
     * @param string $text
     * @return string
     */
    public function encode(string $text): string
    {
        if (empty($this->algorithms)) {
            throw new \InvalidArgumentException('Something is wrong');
        }
        
        foreach ($this->algorithms as $algorithm) {
            $text = $algorithm->encode($text);
        }
        return $text;
    }
}
