##1. PHP

### Introdução
Você está trabalhando em um componente que é responsável por fazer encode de strings.
Todos os encoders devem implementar a interface EncodingAlgorithm com o método encode($text)
que retorna o texto encodado ou estoura uma exceção em caso de erro.

### Definições
Os encoders estão vazios e contem apenas anotações de onde deve ser feita a implementação,
porém outras partes do código também podem ser alteradas.
O comportamento correto de cada encoder está no comentário de cada método

### Para completar a atividade você precisa:

Finalizar a implementação para as classes abaixo, prevendo que inputs inválidos serão passados para os algorítmos:

- OffsetEncodingAlgorithm
- SubstitutionEncodingAlgorithm
- CompositeEncodingAlgorithm

##2. SQL

Você deve realizar uma modelagem baseada em um formulário exemplo *(você não precisa preencher o formulário)*.
Você encontrará mais detalhes sobre a atividade na descrição do formulário

https://docs.google.com/forms/d/e/1FAIpQLSev9ZID2QtR7tosZdc1r6tQ2GfAX5jlWcQcnYcU0J4YmlQ60w/viewform?gxids=7628